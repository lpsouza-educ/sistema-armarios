using System;
using System.Collections.Generic;

class MenuArmarios
{
    public MenuArmarios()
    {
        bool running = true;
        var db = new Database();
        do
        {
        Console.Clear();
        Console.WriteLine();
        Console.WriteLine("Menu:");
        Console.WriteLine("1) Inserir um novo armario");
        Console.WriteLine("2) Editar os armarios existentes");
        Console.WriteLine("3) Apagar um armario existente");
        Console.WriteLine("0) Voltar ao menu anterior");
        Console.WriteLine();
        Console.Write("Escolha uma opção: ");
        switch (Console.ReadLine())
        {
            case "0":
                // Sair
                running = false;
                break;
            case "1":
                // Inserir um novo armario
                Armario novoArmario = new Armario();
                Console.WriteLine();
                Console.Write("Numero do armario: ");
                novoArmario.Numero = int.Parse(Console.ReadLine());

                db.AdicionaArmario(novoArmario);

                Console.WriteLine();
                Console.WriteLine("Armario inserido!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            case "2":
                // Editar armarios
                List<Armario> editListaArmarios = db.ObtemArmarios();

                Console.WriteLine();
                Console.WriteLine("Lista dos armarios cadastrados");
                Console.WriteLine();
                foreach (Armario armario in editListaArmarios)
                {
                    Console.WriteLine(string.Format("{0}", armario.Numero));
                }
                Console.WriteLine();
                Console.Write("Digite o numero do armario a editar: ");

                Armario editArmario = db.ObtemArmario(int.Parse(Console.ReadLine()));

                Console.WriteLine();
                Console.Write("Novo numero do armario: ");
                editArmario.Numero = int.Parse(Console.ReadLine());

                db.EditaArmario(editArmario);

                Console.WriteLine();
                Console.WriteLine("Armario editado!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            case "3":
                // Apagar um armario existente
                List<Armario> removeListaArmarios = db.ObtemArmarios();

                Console.WriteLine();
                Console.WriteLine("Lista dos armarios cadastrados");
                Console.WriteLine();
                foreach (Armario armario in removeListaArmarios)
                {
                    Console.WriteLine(string.Format("{0}", armario.Numero));
                }
                Console.WriteLine();
                Console.Write("Digite o numero do armario a apagar: ");

                db.RemoveArmario(int.Parse(Console.ReadLine()));

                Console.WriteLine();
                Console.WriteLine("Armario apagado!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            default:
                Console.WriteLine();
                Console.WriteLine("Escolha uma das opções do menu!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
        }

        } while (running);
    }
}