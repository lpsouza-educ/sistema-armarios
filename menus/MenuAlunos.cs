using System;
using System.Collections.Generic;

class MenuAlunos
{
    public MenuAlunos()
    {
        bool running = true;
        var db = new Database();
        do
        {
        Console.Clear();
        Console.WriteLine();
        Console.WriteLine("Menu:");
        Console.WriteLine("1) Inserir um novo aluno");
        Console.WriteLine("2) Editar os alunos existentes");
        Console.WriteLine("3) Apagar um aluno existente");
        Console.WriteLine("0) Voltar ao menu anterior");
        Console.WriteLine();
        Console.Write("Escolha uma opção: ");
        switch (Console.ReadLine())
        {
            case "0":
                // Sair
                running = false;
                break;
            case "1":
                // Inserir um novo aluno
                Aluno novoAluno = new Aluno();
                Console.WriteLine();
                Console.Write("Nome do aluno: ");
                novoAluno.Nome = Console.ReadLine();
                Console.WriteLine("Digite a data de nascimento no seguinte formato: DD/MM/AAAA");
                Console.Write("Data de nascimento: ");
                string novoNasc = Console.ReadLine();

                int novoDia = int.Parse(novoNasc.Substring(0,2));
                int novoMes = int.Parse(novoNasc.Substring(3,2));
                int novoAno = int.Parse(novoNasc.Substring(6,4));

                novoAluno.DataNascimento = new DateTime(novoAno, novoMes, novoDia);

                db.AdicionaAluno(novoAluno);

                Console.WriteLine();
                Console.WriteLine("Aluno inserido!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            case "2":
                // Editar alunos
                List<Aluno> editListaAlunos = db.ObtemAlunos();

                Console.WriteLine();
                Console.WriteLine("Lista dos alunos cadastrados");
                Console.WriteLine();
                foreach (Aluno aluno in editListaAlunos)
                {
                    Console.WriteLine(string.Format("{0} | {1} | {2}", aluno.Id, aluno.Nome, aluno.DataNascimento.ToShortDateString()));
                }
                Console.WriteLine();
                Console.Write("Digite o ID do aluno a editar: ");

                Aluno editAluno = db.ObtemAluno(int.Parse(Console.ReadLine()));

                Console.WriteLine();
                Console.Write("Nome do aluno: ");
                editAluno.Nome = Console.ReadLine();
                Console.WriteLine("Digite a data de nascimento no seguinte formato: DD/MM/AAAA");
                Console.Write("Data de nascimento: ");
                string editNasc = Console.ReadLine();

                int editDia = int.Parse(editNasc.Substring(0,2));
                int editMes = int.Parse(editNasc.Substring(3,2));
                int editAno = int.Parse(editNasc.Substring(6,4));

                editAluno.DataNascimento = new DateTime(editAno, editMes, editDia);

                db.EditaAluno(editAluno);

                Console.WriteLine();
                Console.WriteLine("Aluno editado!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            case "3":
                // Apagar um aluno existente
                List<Aluno> removeListaAlunos = db.ObtemAlunos();

                Console.WriteLine();
                Console.WriteLine("Lista dos alunos cadastrados");
                Console.WriteLine();
                foreach (Aluno aluno in removeListaAlunos)
                {
                    Console.WriteLine(string.Format("{0} | {1} | {2}", aluno.Id, aluno.Nome, aluno.DataNascimento.ToShortDateString()));
                }
                Console.WriteLine();
                Console.Write("Digite o ID do aluno a apagar: ");

                Console.WriteLine();
                if (db.RemoveAluno(int.Parse(Console.ReadLine())))
                {
                    Console.WriteLine("Aluno apagado!");
                }
                else
                {
                    Console.WriteLine("Este aluno possui um armario! Não posso deletar!");
                }

                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            default:
                Console.WriteLine();
                Console.WriteLine("Escolha uma das opções do menu!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
        }

        } while (running);
    }
}