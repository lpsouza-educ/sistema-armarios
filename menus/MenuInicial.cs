using System;

class MenuInicial
{
    public MenuInicial()
    {
        bool running = true;
        do
        {
        Console.Clear();
        Console.WriteLine();
        Console.WriteLine("Bem vindo ao sistema de controle de armarios da IENH!");
        Console.WriteLine();
        Console.WriteLine("Neste sistema você pode:");
        Console.WriteLine("- Cadastrar alunos");
        Console.WriteLine("- Cadastrar armarios");
        Console.WriteLine("- Controlar qual aluno utiliza um armario");
        Console.WriteLine();
        Console.WriteLine("Menu:");
        Console.WriteLine("1) Cadastro de alunos");
        Console.WriteLine("2) Cadastro de armarios");
        Console.WriteLine("3) Controle de utilização dos armários");
        Console.WriteLine("0) Sair do sistema");
        Console.WriteLine();
        Console.Write("Escolha uma opção: ");
        switch (Console.ReadLine())
        {
            case "0":
                // Sair
                running = false;
                Console.WriteLine();
                Console.WriteLine("Obrigado por utilizar nosso sistema!");
                Console.WriteLine();
                break;
            case "1":
                // Cadastro de alunos
                new MenuAlunos();
                break;
            case "2":
                // Cadastro de armarios
                new MenuArmarios();
                break;
            case "3":
                // Controle de utilização dos armarios
                new MenuControleArmarios();
                break;
            default:
                Console.WriteLine();
                Console.WriteLine("Escolha uma das opções do menu!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
        }

        } while (running);
    }
}