using System;
using System.Collections.Generic;

class MenuControleArmarios
{
    public MenuControleArmarios()
    {
        bool running = true;
        var db = new Database();
        do
        {
        Console.Clear();
        Console.WriteLine();
        Console.WriteLine("Menu:");
        Console.WriteLine("1) Ver armários vagos");
        Console.WriteLine("2) Emprestar um armario para um aluno");
        Console.WriteLine("3) Recuperar um armario emprestado");
        Console.WriteLine("0) Voltar ao menu anterior");
        Console.WriteLine();
        Console.Write("Escolha uma opção: ");
        switch (Console.ReadLine())
        {
            case "0":
                // Sair
                running = false;
                break;
            case "1":
                // Ver armarios vagos
                List<Armario> listaArmariosValidar = db.ObtemArmarios();

                Console.WriteLine();
                Console.WriteLine("Lista de armarios livres e ocupados");
                Console.WriteLine();
                foreach (Armario armario in listaArmariosValidar)
                {
                    Console.WriteLine(string.Format("{0} | {1}", armario.Numero, armario.SemAluno() ? "Livre" : "Ocupado"));
                }
                Console.WriteLine();
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            case "2":
                // Emprestar armario
                List<Armario> emprestaListaArmarios = db.ObtemArmarios();

                Console.WriteLine();
                Console.WriteLine("Lista dos armarios livres");
                Console.WriteLine();
                foreach (Armario armario in emprestaListaArmarios)
                {
                    if (armario.SemAluno())
                    {
                        Console.WriteLine(string.Format("{0}", armario.Numero));
                    }
                }
                Console.WriteLine();
                Console.Write("Digite o numero do armario para emprestar: ");

                Armario editArmario = db.ObtemArmario(int.Parse(Console.ReadLine()));

                List<Aluno> listaAlunos = db.ObtemAlunos();

                Console.WriteLine();
                Console.WriteLine("Lista dos alunos cadastrados");
                Console.WriteLine();
                foreach (Aluno aluno in listaAlunos)
                {
                    Console.WriteLine(string.Format("{0} | {1}", aluno.Id, aluno.Nome));
                }
                Console.WriteLine();
                Console.Write("Digite o ID do aluno: ");

                Aluno editAluno = db.ObtemAluno(int.Parse(Console.ReadLine()));

                editArmario.Aluno = editAluno;

                db.EditaArmario(editArmario);

                Console.WriteLine();
                Console.WriteLine("Armario emprestado!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            case "3":
                // Recuperar armario
                List<Armario> recuperaListaArmarios = db.ObtemArmarios();

                Console.WriteLine();
                Console.WriteLine("Lista dos armarios livres");
                Console.WriteLine();
                foreach (Armario armario in recuperaListaArmarios)
                {
                    if (!armario.SemAluno())
                    {
                        Console.WriteLine(string.Format("{0}", armario.Numero));
                    }
                }
                Console.WriteLine();
                Console.Write("Digite o numero do armario para recuperar: ");

                Armario recuperaArmario = db.ObtemArmario(int.Parse(Console.ReadLine()));

                recuperaArmario.Aluno = new Aluno();

                db.EditaArmario(recuperaArmario);

                Console.WriteLine();
                Console.WriteLine("Armario recuperado!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
            default:
                Console.WriteLine();
                Console.WriteLine("Escolha uma das opções do menu!");
                Console.WriteLine("Pressione ENTER para retornar...");
                Console.ReadLine();
                break;
        }

        } while (running);
    }
}