class Armario
{
    public int Id { get; set; }
    public int Numero { get; set; }
    public Aluno Aluno { get; set; }

    public bool SemAluno() {
        bool semAluno = true;
        if (this.Aluno.Id != 0)
        {
            semAluno = false;
        }
        return semAluno;
    }
}