using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

class Database
{
    private MySqlConnection conn;
    public Database()
    {
        conn = new MySqlConnection("server=localhost;user id=root;password=senha;port=3306;database=armarios_db;");
    }

    #region Alunos
    public List<Aluno> ObtemAlunos() {
        List<Aluno> listaAlunos = new List<Aluno>();
        conn.Open();
        MySqlCommand command = new MySqlCommand("SELECT * FROM alunos ORDER BY id", conn);
        MySqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            listaAlunos.Add(new Aluno() {
                Id = (int) reader["id"],
                Nome = (string) reader["nome"],
                DataNascimento = (DateTime) reader["data_nasc"]
            });
        }

        conn.Close();
        return listaAlunos;
    }
    public Aluno ObtemAluno(int id) {
        Aluno aluno = new Aluno();
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("SELECT * FROM alunos WHERE id = {0}", id), conn);
        MySqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            aluno = new Aluno() {
                Id = (int) reader["id"],
                Nome = (string) reader["nome"],
                DataNascimento = (DateTime) reader["data_nasc"]
            };
        }

        conn.Close();
        return aluno;
    }
    public void EditaAluno(Aluno aluno) {
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("UPDATE alunos SET nome = '{0}', data_nasc = '{1}' WHERE id = {2}", aluno.Nome, string.Join("-", aluno.DataNascimento.Year, aluno.DataNascimento.Month, aluno.DataNascimento.Day), aluno.Id), conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
    public void AdicionaAluno(Aluno aluno) {
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("INSERT INTO alunos (nome, data_nasc) VALUES ('{0}', '{1}')", aluno.Nome, string.Join("-", aluno.DataNascimento.Year, aluno.DataNascimento.Month, aluno.DataNascimento.Day)), conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
    public bool RemoveAluno(int id) {
        conn.Open();
        MySqlCommand commandTesteAluno = new MySqlCommand(string.Format("SELECT id FROM armarios WHERE aluno_id = {0}", id), conn);
        MySqlDataReader testeAluno = commandTesteAluno.ExecuteReader();

        bool naoTemArmario = true;

        while (testeAluno.Read())
        {
            if ((int) testeAluno["id"] != 0)
            {
                naoTemArmario = false;
            }            
        }
        
        testeAluno.Close();

        if (naoTemArmario) // se o aluno não esta com um armario
        {
            MySqlCommand command = new MySqlCommand(string.Format("DELETE FROM alunos WHERE id = {0}", id), conn);
            command.ExecuteNonQuery();
        }
        conn.Close();

        return naoTemArmario;
    }
    #endregion

    #region Armarios
    public List<Armario> ObtemArmarios() {
        List<Armario> listaArmarios = new List<Armario>();
        conn.Open();
        MySqlCommand command = new MySqlCommand("SELECT armarios.*, alunos.nome, alunos.data_nasc FROM armarios LEFT JOIN alunos ON armarios.aluno_id = alunos.id ORDER BY id", conn);
        MySqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            listaArmarios.Add(new Armario() {
                Id = (int) reader["id"],
                Numero = (int) reader["numero"],
                Aluno = new Aluno() {
                    Id = (int) reader["aluno_id"],
                    Nome = reader["nome"] as string ?? string.Empty,
                    DataNascimento = reader["data_nasc"] as DateTime? ?? new DateTime()
                }
            });
        }

        conn.Close();
        return listaArmarios;
    }
    public Armario ObtemArmario(int numero) {
        Armario armario = new Armario();
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("SELECT armarios.*, alunos.nome, alunos.data_nasc FROM armarios LEFT JOIN alunos ON armarios.aluno_id = alunos.id WHERE numero = {0}", numero), conn);
        MySqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            armario = new Armario() {
                Id = (int) reader["id"],
                Numero = (int) reader["numero"],
                Aluno = new Aluno() {
                    Id = (int) reader["aluno_id"],
                    Nome = reader["nome"] as string ?? string.Empty,
                    DataNascimento = reader["data_nasc"] as DateTime? ?? new DateTime()
                }
            };
        }

        conn.Close();
        return armario;
    }
    public void EditaArmario(Armario armario) {
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("UPDATE armarios SET numero = '{0}', aluno_id = '{1}' WHERE id = {2}", armario.Numero, armario.Aluno.Id, armario.Id), conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
    public void AdicionaArmario(Armario armario) {
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("INSERT INTO armarios (numero, aluno_id) VALUES ({0}, 0)", armario.Numero), conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
    public void RemoveArmario(int numero) {
        conn.Open();
        MySqlCommand command = new MySqlCommand(string.Format("DELETE FROM armarios WHERE numero = {0}", numero), conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
    #endregion
}
